package ru.tinkoff.fintech.homeworks.homework3

import cats.Eq
import cats.implicits._
import ru.tinkoff.fintech.homeworks.Homeworks._

import scala.annotation.tailrec
import scala.collection.immutable.Queue

trait TakeFirst[F[_]] {
  // Возвращает пару из первого элемента коллекции и остальной ее части, если это возможно
  def takeFirst[A](f: F[A]): Option[(A, F[A])]
}

object taske_take_first extends App {
  def equalWithOrder[F[_]: TakeFirst,
                     G[_]: TakeFirst,
                     A: Eq](fa: F[A], ga: G[A]): Boolean = {
    import syntax.takeFirst._

    @tailrec
    def go(f: F[A], g: G[A]): Boolean =
      (f.takeFirst, g.takeFirst) match {
        case (Some((a, f)), Some((b, g))) if a === b => go(f, g)
        case (None, None)                            => true
        case _                                       => false
      }

    go(fa, ga)
  }

  import instances.takeFirst._

  val l1 = List(1, 2)
  val q1 = Queue.empty[Int].enqueue(1).enqueue(2)
  assert(equalWithOrder(l1, q1))

  val l2 = List(1, 2, 3)
  val q2 = Queue.empty[Int].enqueue(2).enqueue(1)
  assert(!equalWithOrder(l2, q2))

  val l3 = List.empty[Int]
  val q3 = Queue.empty[Int]
  assert(equalWithOrder(l3, q3))

}

object instances {
  object takeFirst {
    implicit val takeFirstForList: TakeFirst[List] = task"реализуйте TakeFirst для List"(3, 3, 1)

    implicit val takeFirstForQueue: TakeFirst[Queue] = task"реализуйте TakeFirst для Queue"(3, 3, 2)
  }
}

object syntax {
  object takeFirst {
    implicit class TakeOneOps[F[_], A](val f: F[A]) extends AnyVal {
      def takeFirst(implicit t: TakeFirst[F]): Option[(A, F[A])] = t.takeFirst(f)
    }
  }
}
