package ru.tinkoff.fintech.homeworks.homework3

import ru.tinkoff.fintech.homeworks.Homeworks._
import cats.{Eval, Foldable, Order}
import cats.syntax.all._
import cats.instances.int._

object task2_foldable extends App {
  sealed trait Tree[T] {
    // Тайпкласс Order позволяет сравнивать инстансы типа `T`
    def insert(elem: T)(implicit order: Order[T]): Tree[T]
  }

  case class Node[T](value: T, left: Tree[T] = Leaf[T](), right: Tree[T] = Leaf[T]()) extends Tree[T] {
    def insert(newItem: T)(implicit order: Order[T]): Tree[T] =
      if (newItem < value)
        this.copy(left = left.insert(newItem))
      else
        this.copy(right = right.insert(newItem))
  }

  case class Leaf[T]() extends Tree[T] {
    def insert(elem: T)(implicit order: Order[T]): Tree[T] = Node(elem)
  }

  object Tree {
    def apply[T: Order](values: T*): Tree[T] = values.foldLeft(Leaf[T](): Tree[T]) { case (tree, elem) => tree.insert(elem) }

    // https://typelevel.org/cats/typeclasses/foldable.html
    implicit val itTreeFoldable: Foldable[Tree] = task"Реализуйте cats.Foldable для бинарного дерева"(3, 2, 1)
  }


  val tree = Tree(1, 2, 3, 4, 5, 6)
  assert(tree.contains_(1))
  assert(tree.exists(_ % 2 == 0))
  assert(!tree.forall(_ % 2 == 0))
  assert(tree.size == 6)
  assert(tree.nonEmpty)
  assert(tree.foldLeft(0)(_ + _) == 21)
  assert(tree.reduceLeftOption(_ + _).contains(21))
}
