package ru.tinkoff.fintech.homeworks.homework3

object task1_show_eq extends App {

  sealed trait Rank
  case object Junior extends Rank
  case object Middle extends Rank
  case object Senior extends Rank
  
  object Rank {}

  case class Developer(name: String, rank: Rank)

  object Developer {}

  // Реализуйте тайпклассы cats.Show и cats.Eq для типа Developer.
  // Не используйте isInstanceOf, asInstanceOf и оператор `==`.
  // Используйте pattern-matching и инстансы для других типов.
  // Инстансы тайпклассов должны быть доступны из любого места программы.

//  // Раскомментируйте после написания тайпклассов и необходимых импортов
//  assert(Developer("Yakov", Middle) === Developer("Yakov", Middle))
//  assert(Developer("Yakov", Middle) =!= Developer("Yako1v", Middle))
//  println(Developer("Yakov", Middle).show)
//  assert(Developer("Yakov", Middle).show === "Developer{name=Yakov,rank=Middle}")
}
