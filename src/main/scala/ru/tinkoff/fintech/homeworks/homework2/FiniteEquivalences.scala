package ru.tinkoff.fintech.homeworks.homework2
import ru.tinkoff.fintech.homeworks.Homeworks._

sealed trait Ternary
object Ternary {
  case object Yes   extends Ternary
  case object No    extends Ternary
  case object Maybe extends Ternary
}

case class Equivalent[A, B](to: A => B)(val from : B => A)

object FiniteEquivalences {
  def boolToThree: Equivalent[Boolean => Ternary, (Ternary, Ternary)] =
    task"определите эквивалентность экспоненты в булевый тип и произведения 3 ^ 2 = 3 * 3 " (2, 4, 1)
  def threeToBool: Equivalent[Ternary => Boolean, (Boolean, Boolean, Boolean)] =
    task"определите эквивалентность экспоненты в тернарный тип и произведения 2 ^ 3 = 2 * 2 * 2" (2, 4, 2)

  def boolToBoolToBool: Equivalent[Boolean => Boolean => Boolean, (Boolean => Boolean) => Boolean] =
    task"определите эквивалентность двух экпонент высшего порядка булевых типов (2 ^ 2) ^ 2 = 2 ^ (2 ^ 2)" (2, 4, 3)

  def AToUnit[A]: Equivalent[A => Unit, Unit] =
    task"докажите тривиальное качество терминального типа Unit 1 ^ A = 1" (2, 4, 4)

  def unitToA[A]: Equivalent[Unit => A, A] =
    task"докажите тривиальность экспоненты с единичным показателем A ^ 1 = A" (2, 4, 5)

  def patternMatching[A, B, C] : Equivalent[(A Either B) => C, (A => C, B => C)] =
    task"докажите фундаментальное качество паттерн-матчинга C ^ (A + B) = C ^ A * C ^ B" (2, 4, 6)
}
