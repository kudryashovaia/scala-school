package ru.tinkoff.fintech.homeworks
package homework2
import Homeworks._
import PrefixTree._

sealed trait PrefixTree[+A] {
  def get(index: Seq[Boolean]): Option[A] = this match {
    case Empty    => None
    case Value(x) => Some(x)
    case Branch(left, right) =>
      index match {
        case false +: rest => left.get(rest)
        case true +: rest  => right.get(rest)
      }
  }

  def insert[A1 >: A](x: A1, index: Seq[Boolean]): PrefixTree[A1] = index match {
    case Seq() => Value(x)
    case false +: rest =>
      this match {
        case Branch(left, right) => Branch(left.insert(x, rest), right)
        case _                   => Branch(Empty.insert(x, rest), Empty)
      }
    case true +: rest =>
      this match {
        case Branch(left, right) => Branch(left, right.insert(x, rest))
        case _                   => Branch(Empty, Empty.insert(x, rest))
      }
  }

  def remove(index: Seq[Boolean]): PrefixTree[A] = index match {
    case Seq() => Empty
    case false +: rest =>
      this match {
        case Branch(left, right) => Branch(left.remove(rest), right)
        case other               => other
      }
    case true +: rest =>
      this match {
        case Branch(left, right) => Branch(left, right.remove(rest))
        case other               => other
      }
  }

  def exists(p: A => Boolean): Boolean = task"реализуйте как рекурсивный обход" (2, 3, 1)

  def forall(p: A => Boolean): Boolean = task"реализуйте как рекурсивный обход" (2, 3, 2)

  def collectFirst[B](p: PartialFunction[A, B]): Option[B] = task"реализуйте как рекурсивный обход" (2, 3, 3)

  def map[B](f: A => B): PrefixTree[B] = task"реализуйте как рекурсивный обход с восстановлением структуры" (2, 3, 6)

  def reversed: PrefixTree[A] =
    task"реализуйте как рекурсивный обход с восстановлением структуры в обратном порядке" (2, 3, 7)

  def iterator: Iterator[A] =
    task"""реализуйте как рекурсивный обход со склеиванием итераторов через ++""" (2, 3, 10)

  def dropBefore(index: Seq[Boolean]): PrefixTree[A] =
    task"""попробуйте реализовать выбрасывание всех элементов до данного индекса не включительно""" (2, 3, 11)

  def dropAfter(index: Seq[Boolean]): PrefixTree[A] =
    task"""попробуйте реализовать выбрасывание всех элементов после данного индекса не включительно""" (2, 3, 12)
}
object PrefixTree {
  case object Empty                                                      extends PrefixTree[Nothing]
  final case class Branch[+A](left: PrefixTree[A], right: PrefixTree[A]) extends PrefixTree[A]
  final case class Value[+A](value: A)                                   extends PrefixTree[A]
}
