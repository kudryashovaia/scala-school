package ru.tinkoff.fintech.homeworks.homework2
import ru.tinkoff.fintech.homeworks.Homeworks._

object ListOps {
  def unfoldAccum[S, A](start: S)(f: S => Option[(A, S)]): (List[A], S) =
    task"реализуйте генерацию списка подобную List.unfold, но возвращающую последнее состояние аккумулятора" (2, 1, 1)

  def foldLeftEither[A, B, C, D](list: List[Either[A, B]])(c: C, d: D)(f: (C, A) => C)(g: (D, B) => D): (C, D) =
    task"реализуйте свёртку списка, складывающую элементы в независимые аккумуляторы" (2, 1, 2)
}

object ListOpsHard {
  def listTailRecM[A, B](a: A)(f: A => List[Either[A, B]]): List[B] =
    task"""реализуйте стекобезопасное построение списка так, 
          | чтобы результат этой функции был эквивалентент 
          | f(a).flatMap{
          |   case Left(a) => listTailRecM(a)(f)
          |   case Right(b) => List(b)
          | }
          |""" (2, 1, 3)
}
