package ru.tinkoff.fintech.homeworks
package homework2

import scala.collection.generic.DefaultSerializable
import scala.collection.{
  AbstractSeq,
  IndexedSeqOps,
  IterableFactoryDefaults,
  SeqFactory,
  StrictOptimizedSeqFactory,
  StrictOptimizedSeqOps,
  mutable
}
import Homeworks._

import TrieVec.intToSeq

class TrieVec[+A] private (from: Int, until: Int, tree: PrefixTree[A])
    extends AbstractSeq[A] with IndexedSeq[A] with IndexedSeqOps[A, TrieVec, TrieVec[A]]
    with StrictOptimizedSeqOps[A, TrieVec, TrieVec[A]] with IterableFactoryDefaults[A, TrieVec]
    with DefaultSerializable {

  def apply(i: Int): A = tree.get(intToSeq(i + from)).getOrElse(throw new NoSuchElementException)

  def length: Int = until - from

  override def prepended[B >: A](elem: B): TrieVec[B] =
    new TrieVec(from - 1, until, tree.insert(elem, intToSeq(from - 1)))

  override def appended[B >: A](elem: B): TrieVec[B] =
    new TrieVec(from, until + 1, tree.insert(elem, intToSeq(until)))

  override def tail: TrieVec[A] =
    if (nonEmpty) new TrieVec(from + 1, until, tree.remove(intToSeq(from)))
    else
      throw new UnsupportedOperationException("tail of empty TrieVec")

  override def init: TrieVec[A] =
    if (nonEmpty) new TrieVec(from, until - 1, tree.remove(intToSeq(until - 1)))
    else
      throw new UnsupportedOperationException("init of empty TrieVec")

  override def iterableFactory: SeqFactory[TrieVec] = TrieVec
  override def drop(n: Int): TrieVec[A] =
    if (n >= length) empty else new TrieVec(from + n, until, tree.dropBefore(intToSeq(from + n)))
  override def dropRight(n: Int): TrieVec[A] =
    if (n >= length) empty else new TrieVec(from, until - n, tree.dropAfter(intToSeq(until - n - 1)))

  override def slice(from: Int, until: Int): TrieVec[A] = take(until).drop(from)
  override protected[this] def className: String        = "TrieVec"

  // вы можете закомментировать любой из методов, если хотите воспользоваться стандартной реализацией
  // закомментируейте сначала iterator, чтобы пройти базовые проверки

  override def exists(p: A => Boolean): Boolean = tree.exists(p)

  override def forall(p: A => Boolean): Boolean = tree.forall(p)

  override def collectFirst[B](pf: PartialFunction[A, B]): Option[B] = tree.collectFirst(pf)

  override def take(n: Int): TrieVec[A] = task"реализуйте take через dropRight" (2, 3, 4)

  override def takeRight(n: Int): TrieVec[A] = task"реализуйте takeRight через drop" (2, 3, 5)

  override def map[B](f: A => B): TrieVec[B] = new TrieVec(from, until, tree.map(f))

//  override def iterator: Iterator[A] = tree.iterator

  override def reverse: TrieVec[A] = new TrieVec(
    task"какой должен быть первый индекс здесь?" (2, 3, 8),
    task"какой должен быть второй индекс здесь?" (2, 3, 9),
    tree.reversed
  )

}

object TrieVec extends StrictOptimizedSeqFactory[TrieVec] {
  def intToSeq(x: Int): Seq[Boolean] = {
    val xl = if (x >= 0) x else (1L << 32) + x
    val xb = BigInt(xl)
    31 to 0 by -1 map xb.testBit
  }

  val emptyTrieVec: TrieVec[Nothing] = new TrieVec(0, 0, PrefixTree.Empty)

  def from[A](source: IterableOnce[A]): TrieVec[A] = source.iterator.foldLeft(empty[A])(_ :+ _)
  def empty[A]: TrieVec[A]                         = emptyTrieVec
  def newBuilder[A]: mutable.Builder[A, TrieVec[A]] = new mutable.ImmutableBuilder[A, TrieVec[A]](empty[A]) {
    def addOne(elem: A): this.type = { elems :+= elem; this }
  }
}
