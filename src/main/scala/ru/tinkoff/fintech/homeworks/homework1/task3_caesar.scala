package ru.tinkoff.fintech.homeworks.homework1

object task3_caesar {

  val alphabetCount: Int = 'Z'.toInt - 'A'.toInt + 1

  def encrypt(word: String, offset: Int): String =  word.map{ l =>
    val newLetter = l.toInt + offset
    if (newLetter > 'Z'.toInt)
      (newLetter - alphabetCount).toChar
    else
      newLetter.toChar
  }

  def decrypt(cipher: String, offset: Int): String = cipher.map{ l =>
    val newLetter = l.toInt - offset
    if (newLetter < 'A'.toInt)
      (newLetter + alphabetCount).toChar
    else newLetter.toChar
  }

}