package ru.tinkoff.fintech.homeworks.homework1

object task2_map extends App {
  val map1 = Map("vasily" -> 100, "oleg"     -> 50)
  val map2 = Map("oleg"   -> 40, "alexander" -> 93)

  def unionWith(map1: Map[String, Int], map2: Map[String, Int])(f: (Int, Int) => Int): Map[String, Int] =
    List(map1, map2).reduceLeft ((r, m) => m.foldLeft(r) {
      case (dict, (k, v)) => dict + (k -> f(v, dict.getOrElse(k, 0)))
    })

  assert(unionWith(map1, map2)(_ + _) == Map("vasily" -> 100, "oleg" -> 90, "alexander" -> 93))
}