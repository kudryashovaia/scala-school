
package ru.tinkoff.fintech.homeworks.homework1

object task1_list extends App {
  def getOrder(id: Int): List[String] =
    id match {
      case 1 => List("Apples", "Cherries", "Bananas")
      case 2 => List("Milk", "Yogurt", "Butter", "Salt")
      case 3 => List("Sugar")
      case _ => Nil
    }

  def getItems(ids: List[Int], firstLetter: Char): List[String] = ids.flatMap(getOrder(_).filter(_.startsWith(firstLetter.toString)))

  // Примеры:

  assert(getItems(List(1, 2, 3), 'B') == List("Bananas", "Butter"))
  assert(getItems(List(2, 3, 4), 'B') == List("Butter"))
}