package ru.tinkoff.fintech.homeworks.homework1

object task4_maxima {

  def isLocalMax(f: Int, s: Int, t: Int) = (f <= s) && (t <= s)

  def func (initList: List[Int], newList: List[Int]): List[Int] = {
    initList match {
      case f::s::t::Nil =>
        if(isLocalMax(f,s,t)) newList:+s
        else newList
      case f::s::t::acc =>
        if(isLocalMax(f,s,t)) func(initList.tail, newList:+s)
        else func(initList.tail, newList)
      case _ => List.empty
    }
  }

  def maxima(ints: List[Int]): List[Int] = {
    func(ints, List.empty)
  }
}