
package ru.tinkoff.fintech.homeworks.homework1

object task5_lazy_list extends App {

  lazy val primes: LazyList[Long] =  LazyList.iterate(2){a => a + 1}.filter(a => isPrime(a)).map(_.toLong)

  // Проверка на простоту числа - нужно сделать с помощью `primes`
  def isPrime(n: Long): Boolean = {
    if (n <= 1)
      false
    else if (n == 2)
      true
    else
      !List.range(2,n).exists(x => n % x == 0)
  }
}