package ru.tinkoff.fintech // имена из ru.tinkoff.fintech будут доступны
package lectures.lecture2  // также как и из ru.tinkoff.fintech.lecture.lecture2

// Top-Level scope
// разрешены только object, class, trait

object s1_scopes // объект

package inner {
  // для любителей маленьких неймспейсов как в C++\C#
  object Hello {
    def sayHello(name: String) = println(s"Hello, $name")

    def main(args: Array[String]): Unit = sayHello("inner")
  }
}

trait Outer {
  def main(args: Array[String]) = inner.Hello.sayHello("outer")
}

object OuterMain extends Outer // объекты могут расширять трейты и классы

class SecondOuter(outer: Outer) {
  import inner.Hello._             //в импорте могут участвовать пакеты, объекты, val, параметры
  import outer.{main => outerMain} // общее название : stable identifier

  def runMain(): Unit = {
    outerMain(Array())
    sayHello("second")
  }
}

object SecondOuter extends SecondOuter(OuterMain) {
  def main(args: Array[String]): Unit = runMain()
} // передача параметров в класс при наследовании

object LastMain {
  def main(args: Array[String]): Unit = sayAloha("java2scala")
}
