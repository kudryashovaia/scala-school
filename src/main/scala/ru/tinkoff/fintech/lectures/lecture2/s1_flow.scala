package ru.tinkoff.fintech.lectures.lecture2

object Main1 extends App {
  val nameFirst = "Tinkoff"

  //~~ примерно это
  private[this] var _nameFirst2: String = _

  {
    _nameFirst2 = "Tinkoff"
  }

  def nameFirst2: String = _nameFirst2
  //
  lazy val nameMid = "Fintech" // thread safe now, @volatile in future

  def nameLast = "Java2Scala"

  var countCalled = 0

  private[this] var _countCalled2: Int = _

  {
    _countCalled2 = 0
  }

  def countCalled2 = _countCalled2

  def countCalled2_=(newValue: Int) : Unit = {
    _countCalled2 = newValue
  }

  println(nameFirst + nameMid + nameLast)

  println(
    s"""$nameFirst
       |$nameMid
       |$nameLast""".stripMargin)
}

object Main2 extends App {
  val nameFirst = {
    println("first")
    "Tinkoff"
  }

  lazy val nameMid = {
    println("mid")
    "Fintech"
  }

  def nameLast = {
    println("last")
    "Java2Scala"
  }

  var countCalled = 0

  println("start")

  println(nameFirst + nameMid + nameLast)

  println(s"$nameFirst $nameMid $nameLast")
}
