package ru.tinkoff.fintech.lectures.lecture2

class Foo(x: String, i: Int) {

  override def toString: String = s"Foo(x = $x, i = $i)"
}

object Foo {
  def apply(x: String): Foo = new Foo(x, x.length)
}

abstract class Bar {
  def bar(s: String): String
//  def baz: Int

  def doSomething(u: String, i: Int) = s"bar did $u $i ${bar(u * i)}"
}

object s4_lambdas extends App {
  val func1: Function2[Int, String, String] =
    new Function2[Int, String, String] {
      def apply(v1: Int, v2: String): String = v2 * v1
    }

  val func2: Function2[Int, String, String] =
    (v1: Int, v2: String) => v2 * v1

  val func3: (Int, String) => String = (v1: Int, v2: String) => v2 * v1

  val func4: (Int, String) => String = (v1, v2) => v2 * v1

  val func5 = (v1: Int, v2: String) => v2 * v1
  // ----
  val func5a = (v1: String, v2: Int) => v1 * v2

  val func6: (String, Int) => String = _ * _

  val func7 = (_: String) * (_: Int)

  def impl(x: String, i: Int) = x * i

  val func8: (String, Int) => String = impl

  val func8a: (String, Int) => String = (x, i) => impl(x, i)

  def impl2(z: String)(x: String, i: Int) = (z + x) * i

  val func9: (String, Int) => String = impl2("x")

  val func9a: (String, Int) => String = (x, i) => impl2("x")(x, i)

  def impl3(z: String, x: String, i: Int) = (z + x) * i

  val func10: (String, Int) => String = impl3(_, "z", _)

  println(func8.apply("na", 7) + " batman")
//
//  println(Foo("foo"))
//  //  ||
//  //  V
//  println(Foo.apply("foo"))

  val bar: Bar = (s: String) => s"<<<$s>>>"

  println(bar.doSomething("x", 4))
}
