package ru.tinkoff.fintech.lectures.lecture2

object s3_methods {
  def noparams1 = "no params"

  //alert: side effect!!!
  def noparams2() = {
    println("no params")
    3
  }

  def twoParams(word: String, count: Int) = word * count

  def twoParams2(word: String)(count: Int) = word * count

  def innerDefinitions(word: String): Unit = {
    def anotherMethod(count: Int) = word * count

    val value = anotherMethod(10)

    type Count = Int

    class InnerClass(count: Count) {
      def result = word * count
    }

    lazy val innerResult = new InnerClass(10).result
  }
}
