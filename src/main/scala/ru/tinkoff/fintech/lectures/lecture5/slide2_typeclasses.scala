package ru.tinkoff.fintech.lectures.lecture5

import ru.tinkoff.fintech.lectures.lecture4.slide3_eval.LazyProdStr
import cats.implicits._
import io.circe.{Encoder, Json}
import ru.tinkoff.fintech.lectures.lecture5.slide2_catssyntax.Or.exists
import ru.tinkoff.fintech.lectures.lecture5.slide2_typeclasses.forAll

sealed trait Bin[+A]
object Bin {
  final case class Branch[A](elem: A, left: Bin[A], right: Bin[A]) extends Bin[A]
  case object Leaf                                                 extends Bin[Nothing]

  def one[A](x: A): Bin[A] = Branch(x, Leaf, Leaf)

  val example: Bin[String] = Branch(
    "1",
    one("23"),
    Branch(
      "4",
      one(""),
      Branch("5", one("6"), Leaf)
    )
  )

}
object slide0_motivation extends App {

  def forAll[A](bin: Bin[A])(p: A => Boolean): Boolean = bin match {
    case Bin.Branch(elem, left, right) => p(elem) && forAll(left)(p) && forAll(right)(p)
    case Bin.Leaf                      => true
  }

  def binSum[A](bin: Bin[A])(f: A => Int): Int = bin match {
    case Bin.Branch(elem, left, right) => f(elem) + binSum(left)(f) + binSum(right)(f)
    case Bin.Leaf                      => 0
  }

  def binToList[A, B](bin: Bin[A])(f: A => List[B]): List[B] = bin match {
    case Bin.Branch(elem, left, right) => f(elem) ::: binToList(left)(f) ::: binToList(right)(f)
    case Bin.Leaf                      => Nil
  }

  println(forAll(Bin.example)(_.isEmpty))
  println(forAll(Bin.example)(_.forall(_.isDigit)))
  println(binSum(Bin.example)(_.toIntOption.getOrElse(0)))
  println(binToList(Bin.example)(_.toList))
}

object slide2_generalization {
  def binFoldAccumulate[A, B](bin: Bin[A])(f: A => B)(plus: (B, B) => B)(zero: B): B = bin match {
    case Bin.Branch(elem, left, right) =>
      plus(binFoldAccumulate(left)(f)(plus)(zero), plus(f(elem), binFoldAccumulate(right)(f)(plus)(zero)))
    case Bin.Leaf => zero
  }
  def forAll[A](bin: Bin[A])(p: A => Boolean): Boolean =
    binFoldAccumulate(bin)(p)(_ && _)(true)

  def binSum[A](bin: Bin[A])(f: A => Int): Int =
    binFoldAccumulate(bin)(f)(_ + _)(0)

  def binToList[A, B](bin: Bin[A])(f: A => List[B]): List[B] =
    binFoldAccumulate(bin)(f)(_ ::: _)(Nil)
}

object slide2_abstraction {
  trait Aggregation[A] {
    def plus(x: A, y: A): A
    // plus(zero, x) == x
    def zero: A
  }
  object Aggregation {
    object ofBoolean extends Aggregation[Boolean] {
      def plus(x: Boolean, y: Boolean): Boolean = x && y
      def zero: Boolean                         = true
    }

    object ofInt extends Aggregation[Int] {
      def plus(x: Int, y: Int): Int = x + y
      def zero: Int                 = 0
    }

    def ofList[A] = new Aggregation[List[A]] {
      def plus(x: List[A], y: List[A]): List[A] = x ::: y
      def zero: List[A]                         = Nil
    }
  }

  def binFoldAccumulate[A, B](bin: Bin[A])(f: A => B)(agg: Aggregation[B]): B = bin match {
    case Bin.Branch(elem, left, right) =>
      agg.plus(binFoldAccumulate(left)(f)(agg), agg.plus(f(elem), binFoldAccumulate(right)(f)(agg)))
    case Bin.Leaf => agg.zero
  }
  def forAll[A](bin: Bin[A])(p: A => Boolean): Boolean =
    binFoldAccumulate(bin)(p)(Aggregation.ofBoolean)

  def binSum[A](bin: Bin[A])(f: A => Int): Int =
    binFoldAccumulate(bin)(f)(Aggregation.ofInt)

  def binToList[A, B](bin: Bin[A])(f: A => List[B]): List[B] =
    binFoldAccumulate(bin)(f)(Aggregation.ofList)
}

object slide2_typeclasses extends App {
  trait Monoid[A] {
    def plus(x: A, y: A): A
    // plus(zero, x) == x
    // plus(x, zero) == x
    // plus(x, plus(y, z)) == plus(plus(x, y), z)
    def zero: A
  }

  object Monoid {
    implicit object ofBoolean extends Monoid[Boolean] {
      def plus(x: Boolean, y: Boolean): Boolean = x && y
      def zero: Boolean                         = true
    }

    implicit object ofInt extends Monoid[Int] {
      def plus(x: Int, y: Int): Int = x + y
      def zero: Int                 = 0
    }

    implicit def ofList[A] = new Monoid[List[A]] {
      def plus(x: List[A], y: List[A]): List[A] = x ::: y
      def zero: List[A]                         = Nil
    }
  }

  def binFoldAccumulate[A, B](bin: Bin[A])(f: A => B)(implicit agg: Monoid[B]): B = bin match {
    case Bin.Branch(elem, left, right) =>
      agg.plus(f(elem), agg.plus(binFoldAccumulate(left)(f), binFoldAccumulate(right)(f)))
    case Bin.Leaf => agg.zero
  }
  def forAll[A](bin: Bin[A])(p: A => Boolean): Boolean = binFoldAccumulate(bin)(p)

  def binSum[A](bin: Bin[A])(f: A => Int): Int = binFoldAccumulate(bin)(f)

  def binToList[A, B](bin: Bin[A])(f: A => List[B]): List[B] = binFoldAccumulate(bin)(f)

  println(forAll(Bin.example)(_.isEmpty))
  println(forAll(Bin.example)(_.forall(_.isDigit)))
  println(binSum(Bin.example)(_.toIntOption.getOrElse(0)))
  println(binToList(Bin.example)(_.toList))
}

object slide2_catssyntax extends App {
  import cats.Monoid
  import cats.syntax.monoid._

  def binFoldAccumulate[A, B: Monoid](bin: Bin[A])(f: A => B): B = bin match {
    case Bin.Branch(elem, left, right) =>
      f(elem) |+| binFoldAccumulate(left)(f) |+| binFoldAccumulate(right)(f)
    case Bin.Leaf => Monoid.empty[B]
  }

  def binSum[A](bin: Bin[A])(f: A => Int): Int = binFoldAccumulate(bin)(f)

  def binToList[A, B](bin: Bin[A])(f: A => List[B]): List[B] = binFoldAccumulate(bin)(f)

  println(binSum(Bin.example)(_.toIntOption.getOrElse(0)))
  println(binSum(Bin.example)(_.toIntOption.getOrElse(0)))
  println(binToList(Bin.example)(_.toList))

  final case class All(value: Boolean) extends AnyVal {
    def combine(other: All): All = All(value && other.value)
  }

  object All {
    implicit object allMonoid extends Monoid[All] {
      val empty: All                   = All(true)
      def combine(x: All, y: All): All = All(x.value && y.value)
    }
  }
  final case class Or(value: Boolean) extends AnyVal

  object Or {
    implicit object allMonoid extends Monoid[Or] {
      val empty: Or                 = Or(false)
      def combine(x: Or, y: Or): Or = Or(x.value || y.value)
    }

    def forAll[A](bin: Bin[A])(p: A => Boolean): Boolean = binFoldAccumulate(bin)(x => All(p(x))).value
    def exists[A](bin: Bin[A])(p: A => Boolean): Boolean = binFoldAccumulate(bin)(x => Or(p(x))).value
  }

  println(forAll(Bin.example)(_.isEmpty))
  println(forAll(Bin.example)(_.forall(_.isDigit)))

  println(exists(Bin.example)(_.isEmpty))
  println(exists(Bin.example)(_.exists(_.isLetter)))
}
