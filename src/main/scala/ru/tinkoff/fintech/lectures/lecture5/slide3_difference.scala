package ru.tinkoff.fintech.lectures.lecture5
import cats.Monoid
import cats.implicits._

object slide3_difference extends App {

  // F-bound
  trait Aggregate[A <: Aggregate[A]] { self: A =>
    def plus(other: A): A
  }

  //* РАНЕЕ СВЯЗЫВАНИЕ, ПОЗДНЕЕ ОПРЕДЕЛЕНИЕ

  case class Rational(num: BigInt, den: BigInt) extends Aggregate[Rational] {
    def +(other: Rational): Rational = {
      val g    = den.gcd(other.den)
      val den1 = den / g
      val den2 = other.den / g
      Rational(num * den2 + other.num * den1, den * den2)
    }
    def plus(other: Rational): Rational = this + other
  }

  implicit object rationalMonoid extends Monoid[Rational] {
    val empty: Rational                             = Rational(0, 1)
    def combine(x: Rational, y: Rational): Rational = x + y
  }

  //* ДИСПЕТЧЕРИЗАЦИЯ БЕЗ НАЛИЧИЯ ЗНАЧЕНИЯ

//  println(List(Monoid.empty[Rational]))

  //* СТРОГОЕ СООТВЕТСТВИЕ ТИПОВ
  trait Combine {
    def combine(other: Combine): Combine
  }

  //* ПЕРЕОПРЕДЕЛЕНИЕ ДЛЯ ПАРАМЕТРИЧЕСКИХ ТИПОВ

  final case class Or[N](value: N) extends AnyVal

  object Or {
    implicit val orBoolean: Monoid[Or[Boolean]] = new Monoid[Or[Boolean]] {
      val empty: Or[Boolean]                                   = Or(false)
      def combine(x: Or[Boolean], y: Or[Boolean]): Or[Boolean] = Or(x.value || y.value)
    }

    implicit val orInt: Monoid[Or[Int]] = new Monoid[Or[Int]] {
      val empty: Or[Int]                           = Or(0)
      def combine(x: Or[Int], y: Or[Int]): Or[Int] = Or(x.value | y.value)
    }

    implicit val orString: Monoid[Or[String]] = new Monoid[Or[String]] {
      val empty: Or[String] = Or("")
      def combine(x: Or[String], y: Or[String]): Or[String] =
        Or(
          x.value.iterator
            .zipAll(y.value.iterator, ' ', ' ')
            .map {
              case (' ', c) => c
              case (c, _)   => c
            }
            .mkString
        )
    }

  }

//  println(Or(3) |+| Or(5))
//  println(Or(true) |+| Or(false))
//  println(Or(" E  O  O") |+| Or("hello world"))

  // АВТОМАТИЧЕСКИЙ ВЫВОД

  // Monoid[A], Monoid[B],  Monoid[C] => Monoid[(A, B, C)]
  // Monoid[A]  => Monoid[C => A]

//  println((1, "ok") |+| ((2, "lol")))
//  println(Monoid.empty[(Int, String)])

  val foo: Int => (Int, String, Or[String]) = List[(Int, Int => String)](
    (1, x => x.toString),
    (100, x => s"   $x"),
    (10000, x => s"      $x")
  ).foldMap {
    case (mul, f) => (x: Int) => (x * mul, f(x), Or(f(x)))
  }


  final case class Prod[N](value: N) extends AnyVal

  object Prod {
    import Numeric.Implicits._
    implicit def sumNumeric[N](implicit N: Numeric[N]): Monoid[Prod[N]] = new Monoid[Prod[N]] {
      val empty: Prod[N]                           = Prod(N.one)
      def combine(x: Prod[N], y: Prod[N]): Prod[N] = Prod(x.value * y.value)
    }
  }
//
  println(List.range(1L, 10L).foldMap(Prod(_)))
  println(List.range(BigInt(1), BigInt(1000)).foldMap(Prod(_)))
  println(List.range(1, 100).foldMap(i => Prod(i.toDouble)))

  // ВЫВОД ТИПОВ : ВЫРАЖЕНИЕ => ТИП
  // ВЫВОД ТЕРМОВ : ТИП => ВЫРАЖЕНИЕ

}
