package ru.tinkoff.fintech.lectures.lecture3

import cats.syntax.option._
import cats.syntax.either._

object s5_standard extends App {
  final case class User private[User] (name: String, age: Int, company: String)

  object User {
    def apply(name: String, age: Int, company: String): Either[String, User] =
      if (age < 18) Left("user is too young")
      else if (name.isEmpty) Left("name is empty")
      else Right(new User(name, age, company))
  }

  case class BuildUser(name: Option[String] = None, age: Option[Int] = None, company: Option[String] = None) {
    def toUser: Either[String, User] =
      for {
        nameF    <- name.toRight("name is not defined")
        ageF     <- age.toRight("age is not defined")
        companyF <- company.toRight("company is not defined")
        user     <- User(nameF, ageF, companyF)
      } yield user

  }

}
