package ru.tinkoff.fintech.lectures.lecture3

object s2_sealed {
  sealed trait Organization
  final case class User(firstName: String, lastName: String) extends Organization
  final case class Department(name: String, head: User)      extends Organization

  val user: User = User("Oleg", "Nizhnik")

  val org: Organization = user

  def heads(org: Organization*): Map[String, String] =
    org.collect {
      case Department(name, User(first, last)) => s"$first $last" -> name
    }.toMap

  def users(org: Organization*): List[User] =
    org.map {
      case department: Department => department.head
      case user @ User(_, _)      => user
    }.distinct.toList

}
