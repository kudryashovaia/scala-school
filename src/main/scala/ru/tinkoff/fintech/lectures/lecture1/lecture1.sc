class Animal(val name: String)

class Cow(override val name: String,
          val weight: Double)
  extends Animal(name)

implicitly[Cow <:< Animal]
implicitly[List[Cow] <:< List[Animal]]
implicitly[
  (Animal => String) <:< (Cow => String)
]

val printName: Animal => String =
  cow => s"cow's name is ${cow.name}"

val printWeight: Cow => String =
  cow => s"cow's age is ${cow.weight}"


val res = Array.fill[Int](10)(-3)
def foo(animals: List[Animal]) = animals

def bar(cows: List[Cow]) = foo(cows)


case class Foo(a: String, b: Int)