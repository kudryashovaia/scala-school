package ru.tinkoff.fintech.lectures.lecture1

sealed trait Bool extends Product

object Bool {
  case object True  extends Bool
  case object False extends Bool

  def if_[A](b: Bool)(ifTrue: => A)(ifFalse: => A): A = b match {
    case True  => ifTrue
    case False => ifFalse
  }

  def and(b1: Bool, b2: Bool): Bool = if_(b1)(b2)(False)
  def or(b1: Bool, b2: Bool): Bool  = if_(b1)(True: Bool)(b2)
  def not(b: Bool): Bool            = if_(b)(False: Bool)(True)
}
