package ru.tinkoff.fintech.lectures.lecture1

trait Boole {
  def if_[A](ifTrue: => A)(ifFalse: => A): A
}

object Boole {
  val True = new Boole {
    def if_[A](ifTrue: => A)(ifFalse: => A): A = ifTrue
  }

  val False = new Boole {
    def if_[A](ifTrue: => A)(ifFalse: => A): A = ifFalse
  }

  def not(b: Boole): Boole = new Boole {
    def if_[A](ifTrue: => A)(ifFalse: => A): A = b.if_(ifFalse)(ifTrue)
  }

  def and(b1: Boole, b2: Boole): Boole = new Boole {
    def if_[A](ifTrue: => A)(ifFalse: => A): A =
      b1.if_(b1.if_(ifTrue)(ifFalse))(ifFalse)
  }

  def or(b1: Boole, b2: Boole): Boole = new Boole {
    def if_[A](ifTrue: => A)(ifFalse: => A): A =
      b1.if_(ifTrue)(b2.if_(ifTrue)(ifFalse))
  }
}
