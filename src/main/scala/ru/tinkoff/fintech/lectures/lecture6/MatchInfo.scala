package ru.tinkoff.fintech.lectures.lecture6
import java.time.LocalDate
import java.time.format.DateTimeFormatter

import cats.Show

import scala.util.Try

case class Player(name: String, scores: Short)
case class MatchInfo(date: LocalDate, player1: Player, player2: Player)


object MatchInfo {
  val dateFormat = DateTimeFormatter.ofPattern("MM/dd/yyyy")

  String.format("sdfsdf")

  val scores = "(\\d+)-(\\d+)".r

  object Date {
    def unapply(s: String): Option[LocalDate] = Try(LocalDate.parse(s, dateFormat)).toOption
  }

  object Short {
    def unapply(s: String): Option[Short] = s.toShortOption
  }

  def unapply(s: Seq[String]): Option[MatchInfo] = s match {
    case Seq(Date(date), p1, _, scores(Short(s1), Short(s2)), p2, _, _*) =>
      Some(MatchInfo(date, Player(p1, s1), Player(p2, s2)))
    case _ => None
  }

  implicit val show: Show[MatchInfo] = m => {
    import m._
    f"${player1.name}%12s ${player1.scores}%1d - ${player2.scores}%1d ${player2.name}%-12s [$date]"
  }
}
