package ru.tinkoff.fintech.lectures.lecture6
import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousFileChannel, CompletionHandler}
import java.nio.charset.StandardCharsets
import java.nio.file.Paths

import akka.actor.typed.scaladsl.Behaviors
import akka.actor.typed.{ActorRef, ActorSystem, Behavior}
import cats.syntax.show._

object slide2_actors {

  val FileName = "sc2-matches-history.csv"

  def matchReport = Behaviors.receiveMessage[MatchInfo] { info =>
    println(info.show)
    Behaviors.same
  }

  def handleChunk(reporter: ActorRef[MatchInfo], rem: String = ""): Behavior[String] =
    Behaviors.receiveMessage[String] { s =>
      val lines :+ newRem = (rem + s).split("\n").toSeq
      lines.view
        .map(_.split(",").toSeq)
        .collect { case MatchInfo(info) => info }
        .foreach(reporter ! _)

      handleChunk(reporter, newRem)
    }

  // связан обратной связью с Loop актором
  // можно избежать утечки абстракции, но для этого нужно запустить промежуточный актор,
  // выполняющий преобразование Long => Loop.Continue
  def reader(fileName: String, receiver: ActorRef[String], loop: ActorRef[Loop]) =
    Behaviors.setup[Long] { ctx =>
      val channel = AsynchronousFileChannel.open(Paths.get("sc2-matches-history.csv"))
      val buffer  = ByteBuffer.allocate(1000)

      Behaviors.receiveMessage { pos =>
        val handler = new CompletionHandler[Integer, Unit] {
          def completed(v: Integer, a: Unit): Unit =
            if (v <= 0) loop ! Loop.Stop
            else {
              val str = new String(buffer.array(), 0, v, StandardCharsets.US_ASCII)
              receiver ! str
              buffer.clear()
              loop ! Loop.Continue(pos + str.length)
            }
          def failed(err: Throwable, a: Unit): Unit = Behaviors.stopped
        }

        channel.read[Unit](buffer, pos, (), handler)
        Behaviors.same
      }
    }

  sealed trait Loop
  object Loop {
    case object Start                    extends Loop
    final case class Continue(pos: Long) extends Loop
    case object Stop                     extends Loop

    def apply(fileName: String) = Behaviors.setup[Loop] { ctx =>
      val reporter = ctx.spawn(matchReport, "reporter")
      val chunks   = ctx.spawn(handleChunk(reporter), "chunks")
      val fileRead = ctx.spawn(reader(fileName, chunks, ctx.self), "fileRead")

      Behaviors.receiveMessage {
        case Start =>
          fileRead ! 0
          Behaviors.same
        case Continue(pos) =>
          fileRead ! pos
          Behaviors.same
        case Stop =>
          Behaviors.stopped
      }
    }
  }

  def main(args: Array[String]): Unit = {
    val root = ActorSystem(Loop(FileName), "matchLoop")
    root ! Loop.Start
  }

}
