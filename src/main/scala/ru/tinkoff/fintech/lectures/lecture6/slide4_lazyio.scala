package ru.tinkoff.fintech.lectures.lecture6
import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousFileChannel, CompletionHandler}
import java.nio.charset.StandardCharsets
import java.nio.file.Paths

import cats.syntax.show._
import zio._
import zio.console._

case class ZIOReader(channel: AsynchronousFileChannel, buffer: ByteBuffer) {

  def read(position: Long): Task[Option[String]] = Task.effectAsync[Option[String]] { k =>
    val handler = new CompletionHandler[Integer, Unit] {
      def completed(v: Integer, a: Unit): Unit =
        if (v <= 0) k(ZIO.succeed(None))
        else {
          val str = new String(buffer.array(), 0, v, StandardCharsets.US_ASCII)
          buffer.clear()
          k(ZIO.succeed(Some(str)))
        }
      def failed(err: Throwable, a: Unit): Unit = k(Task.fail(err))
    }
    channel.read[Unit](buffer, position, (), handler)
  }
}

object ZIOReader {
  def make(fileName: String) = for {
    chan <- Task.effect(AsynchronousFileChannel.open(Paths.get(fileName)))
    buf  <- Task.effect(ByteBuffer.allocate(1000))
  } yield ZIOReader(chan, buf)
}

object slide4_lazyio extends zio.App {

  val FileName = "sc2-matches-history.csv"

  def dispatchResult(info: MatchInfo) = putStrLn(info.show)

  def handleChunk(s: String): URIO[Console, String] = {
    val lines :+ rem = s.split("\n").toSeq
    URIO.foreach_(lines.view.map(_.split(",").toSeq)) {
      case MatchInfo(info) => dispatchResult(info)
      case _               => URIO.unit
    } as rem
  }

  // рекурсивный flatMap
  // мы не боимся stackoverflow здесь
  // но для того, чтобы не утекала память на очень длинных циклах нужно использовать https://github.com/oleg-py/better-monadic-for
  def loop(reader: ZIOReader, pos: Int = 0, rem: String = ""): ZIO[Console, Option[Throwable], Unit] =
    for {
      str     <- reader.read(pos).some
      remNext <- handleChunk(rem + str)
      _       <- loop(reader, pos + str.length, remNext)
    } yield ()

  def proc: RIO[Console, Unit] = for {
    reader <- ZIOReader.make(FileName)
    _      <- loop(reader).catchAll(Task.foreach(_)(Task.fail))
  } yield ()

  def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    proc.catchAllCause(c => putStrLn(c.prettyPrint)) as 0
}
