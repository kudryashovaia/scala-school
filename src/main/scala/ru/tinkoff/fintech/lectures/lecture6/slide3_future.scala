package ru.tinkoff.fintech.lectures.lecture6
import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousFileChannel, CompletionHandler}
import java.nio.charset.StandardCharsets
import java.nio.file.Paths

import cats.syntax.show._

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, ExecutionContext, Future, Promise}

case class FutureReader(channel: AsynchronousFileChannel, buffer: ByteBuffer) {
  def read(position: Long): Future[Option[String]] = {
    val p = Promise[Option[String]]

    val handler = new CompletionHandler[Integer, Unit] {
      def completed(v: Integer, a: Unit): Unit =
        if (v <= 0) p.success(None)
        else {
          val str = new String(buffer.array(), 0, v, StandardCharsets.US_ASCII)
          buffer.clear()
          p.success(Some(str))
        }
      def failed(err: Throwable, a: Unit): Unit = p.failure(err)
    }
    channel.read[Unit](buffer, position, (), handler)

    p.future
  }
}

object FutureReader {
  def apply(fileName: String): FutureReader = {
    val chan = AsynchronousFileChannel.open(Paths.get(fileName))
    val buf  = ByteBuffer.allocate(1000)
    FutureReader(chan, buf)
  }
}

object slide2_future {
  val FileName = "sc2-matches-history.csv"

  def dispatchResult(info: MatchInfo)(implicit ec: ExecutionContext) = Future(println(info.show))

  def handleChunk(s: String)(implicit ec: ExecutionContext): Future[String] = {
    val lines :+ rem = s.split("\n").toSeq
    Future
      .traverse(lines.view.map(_.split(",").toSeq).to(LazyList)) {
        case MatchInfo(info) => dispatchResult(info)
        case _               => Future.unit
      }
      .map(_ => rem)
  }

  // рекурсивный flatMap
  // мы не боимся stackoverflow здесь
  // но для того, чтобы не утекала память на очень длинных циклах нужно использовать https://github.com/oleg-py/better-monadic-for
  def loop(reader: FutureReader, pos: Int = 0, rem: String = "")(implicit ec: ExecutionContext): Future[Unit] =
    reader.read(pos).flatMap {
      case None      => Future.unit
      case Some(str) => handleChunk(rem + str).flatMap(loop(reader, pos + str.length, _))
    }

  def main(args: Array[String]): Unit = {
    import ExecutionContext.Implicits.global

    val reader = FutureReader(FileName)
    Await.result(loop(reader), Duration.Inf)
  }
}
