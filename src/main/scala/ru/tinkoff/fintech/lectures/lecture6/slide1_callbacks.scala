package ru.tinkoff.fintech.lectures.lecture6
import java.nio.ByteBuffer
import java.nio.channels.{AsynchronousFileChannel, CompletionHandler}
import java.nio.charset.StandardCharsets
import java.nio.file.Paths

import scala.concurrent.duration.Duration
import scala.concurrent.{Await, Promise}
import scala.util.{Failure, Success, Try}
import cats.syntax.show._

class CallbackReader(channel: AsynchronousFileChannel, bufSize: Int = 1000) {
  private val buffer = ByteBuffer.allocate(bufSize)

  def read(pos: Long)(f: Try[Option[String]] => Unit) = channel.read[Unit](
    buffer,
    pos,
    (),
    new CompletionHandler[Integer, Unit] {
      def completed(v: Integer, a: Unit): Unit =
        if (v <= 0) f(Success(None))
        else {
          val str = new String(buffer.array(), 0, v, StandardCharsets.US_ASCII)
          buffer.clear()
          f(util.Success(Some(str)))
        }
      def failed(err: Throwable, a: Unit): Unit = f(util.Failure(err))
    }
  )
}

object slide1_callbacks extends App {
  val channel = AsynchronousFileChannel.open(Paths.get("sc2-matches-history.csv"))

  val reader = new CallbackReader(channel)

  val lock = Promise[Unit]()

  def handleChunk(s: String): String = {
    val lines :+ rem = s.split("\n").toSeq
    for (line <- lines)
      line.split(",").toSeq match {
        case MatchInfo(info) => println(info.show)
        case _               =>
      }

    rem
  }

  def readLoop(position: Long = 0, rem: String = ""): Unit =
    reader.read(position) {
      case Success(Some(str)) =>
        val newRem = handleChunk(rem + str)
        readLoop(position + str.length, newRem)
      case Success(None) =>
        println("Done")
        lock.success(())
      case Failure(ex) =>
        lock.failure(ex)

    }

  readLoop()

  Await.ready(lock.future, Duration.Inf)

}
