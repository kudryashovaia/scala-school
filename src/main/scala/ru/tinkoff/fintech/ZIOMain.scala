package ru.tinkoff.fintech
import zio.console._

object ZIOMain extends zio.App {
  def greet =
    for {
      _    <- putStrLn("Enter your name:")
      name <- getStrLn
      _    <- putStrLn(s"Hello $name!!!")
    } yield 0

  def run(args: List[String]) = greet.ignore as 1
}
