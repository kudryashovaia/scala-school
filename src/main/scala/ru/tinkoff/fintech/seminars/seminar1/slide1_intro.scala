package ru.tinkoff.fintech.seminars.seminar1


object slide1_intro extends App{

  // List - неизменяемая коллекция
  val list: List[Int] = List(1, 2, 3)

  val prependedList: List[Int] = 0 :: list

  val updatedList: List[Int] = prependedList.updated(2, 42)

  println(list)
  println(prependedList)
  println(updatedList)

}
