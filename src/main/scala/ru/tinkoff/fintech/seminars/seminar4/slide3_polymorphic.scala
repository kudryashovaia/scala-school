package ru.tinkoff.fintech.seminars.seminar4

import cats.effect.SyncIO
import cats.{Eval, Monad, MonadError}

import scala.annotation.tailrec
import scala.util.Random

object slide3_monad extends App {

  sealed trait MyOption[+A]

  case class MySome[A](a: A) extends MyOption[A]

  case object MyNone extends MyOption[Nothing]

  implicit object myOptionMonad extends Monad[MyOption] {
    def flatMap[A, B](fa: MyOption[A])(f: A => MyOption[B]): MyOption[B] =
      fa match {
        case MySome(a) => f(a)
        case MyNone => MyNone
      }

    @tailrec
    def tailRecM[A, B](a: A)(f: A => MyOption[Either[A, B]]): MyOption[B] = f(a) match {
      case MyNone => MyNone
      case MySome(Left(nextA)) => tailRecM(nextA)(f)
      case MySome(Right(b)) => MySome(b)
    }

    def pure[A](x: A): MyOption[A] = MySome(x)
  }

  import cats.syntax.all._

  val someInt = 1.pure[MyOption]
  println(someInt)
  println(someInt.map(_ * 2))
  println(someInt.flatMap(x => MySome(x * 4)))
  println(someInt.flatMap(x => MyNone))

  println(Eval.later("Hello world").flatTap(x => Eval.later(println(x + "!!!"))).value)

  val randomInt = Eval.always(Random.nextInt(10))
  randomInt.flatTap(x => Eval.later(println(x))).iterateWhile(_ != 0).value
}

object slide3_monad_errors extends App {

  import cats.instances.either._
  import cats.syntax.all._

  type EitherError[A] = Either[Throwable, A]
  type MonadThrow[F[_]] = MonadError[F, Throwable]

  def parseInt[F[_] : MonadThrow](string: String): F[Int] =
    string.toIntOption match {
      case Some(a) => a.pure[F]
      case None => MonadError[F, Throwable].raiseError(new Error(s"$string is not an int"))
    }

  case class MyError(text: String) extends Error(text)

  def takeHead[F[_] : MonadThrow](string: String): F[String] =
    string.headOption.map(_.toString) match {
      case Some(a) => a.pure[F]
      case None => MonadError[F, Throwable].raiseError(MyError(s"$string is too short"))
    }

  def doSomething[F[_] : MonadThrow](string: String): F[String] =
    takeHead[F](string)
      .flatMap(parseInt[F])
      .map(_.toString)

  println("Either:")
  println(doSomething[EitherError]("12345"))
  println(doSomething[EitherError]("abcbc"))
  println(doSomething[EitherError](""))
  println()

  println("SyncIO:")
  // handleError - преобразовать ошибку в "нормальное" значение (если `F[A]`, то в `A`)
  println(doSomething[SyncIO]("12345").handleError(error => s"Error happened: $error").unsafeRunSync())
  println(doSomething[SyncIO]("abcbc").handleError(error => s"Error happened: $error").unsafeRunSync())
  println(doSomething[SyncIO]("").handleError(error => s"Error happened: $error").unsafeRunSync())
  println()

  println(".handleErrorWith:")
  // handleErrorWith - как handleError, но принимает функцию, которая возвращает F[A]:
  println(doSomething[SyncIO]("abcbc")
    .handleErrorWith(error => takeHead[SyncIO](error.getMessage))
    .unsafeRunSync())
  println()

  println(".attempt:")
  // attempt - получить `F[Either[E, A]]` из `F[A]`, `E` - тип ошибки, в данном случае Throwable
  println(doSomething[SyncIO]("12345").attempt.unsafeRunSync())
  println(doSomething[SyncIO]("abcbc").attempt.unsafeRunSync())
  println()

  println(".adaptError:")
  // adaptError - преобразовать ошибку в другую ошибку того же типа, используя частичную функцию
  println(doSomething[SyncIO]("abcbc")
    .adaptError({case MyError(text) => new Error(s"MyError happened: $text")})
    .attempt
    .unsafeRunSync())
  println(doSomething[SyncIO]("")
    .adaptError({case MyError(text) => new Error(s"MyError happened: $text")})
    .attempt
    .unsafeRunSync())
  println()

  println(".onError:")
  // onError - сделать какой-то side-effect, не изменяя ошибку
  println(doSomething[SyncIO]("abcbc")
    .onError{case error => SyncIO(println(s"Error happened: $error"))}
    .attempt
    .unsafeRunSync())

  println(".onError:")
  // onError - сделать какой-то side-effect, не изменяя ошибку
  println(doSomething[SyncIO]("abcbc")
    .attempt
    .unsafeRunSync())
}
