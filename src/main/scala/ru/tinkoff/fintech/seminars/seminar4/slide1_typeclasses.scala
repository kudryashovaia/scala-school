package ru.tinkoff.fintech.seminars.seminar4

object slide1_typeclasses extends App {

  trait Monoid[A] {
    def plus(x: A, y: A): A
    def zero: A
  }

  object Monoid {

    implicit object StringMonoid extends Monoid[String] {
      def plus(x: String, y: String): String = x + y
      def zero: String = ""
    }

    implicit def numericMonoid[A](implicit numeric: Numeric[A]): Monoid[A] =
      new Monoid[A] {
        def plus(x: A, y: A): A = numeric.plus(x, y)
        def zero: A = numeric.zero
      }
  }

  def reduce[A](list: List[A])(implicit monoid: Monoid[A]): A =
    list.reduceOption(monoid.plus).getOrElse(monoid.zero)

  def slidingReduce[A: Monoid](list: List[A], size: Int): List[A] =
    list.sliding(size).map(window => reduce(window)).toList

  val emptyList = List.empty[Int]
  val listOfInts = List(1, 2, 3, 4, 5)
  val listOfStrings = List("a", "b", "c", "d", "e")

  println("reduce:")
  println(reduce(emptyList))
  println(reduce(listOfInts))
  println(reduce(listOfStrings))

  println("slidingReduce:")
  println(slidingReduce(listOfInts, 3))
  println(slidingReduce(listOfStrings, 3))

}
