package ru.tinkoff.fintech.seminars.seminar3

import scala.util.matching.Regex

object slide3_task1 extends App {

  case class Email(nickname: String, domain: String)

  val emailReg: Regex = ???

  def parseEmail(email: String): Either[String, Email] = email match {
    // TODO regex matching
    case _ => Left("This is not correct email!")
  }

  println(parseEmail("johndoe@rambler.ru"))

}
