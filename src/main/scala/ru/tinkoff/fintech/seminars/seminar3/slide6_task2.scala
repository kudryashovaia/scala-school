package ru.tinkoff.fintech.seminars.seminar3

sealed trait IntTree {
  def insert(elem: Int): IntTree
}

case class Node(value: Int, left: IntTree = Dummy, right: IntTree = Dummy) extends IntTree {
  override def insert(newItem: Int): IntTree =
    if (newItem < value)
      this.copy(left = left.insert(newItem))
    else
      this.copy(right = right.insert(newItem))
}

case object Dummy extends IntTree {
  override def insert(elem: Int): IntTree = Node(elem)
}

object IntTree {
  def apply(values: Int*): IntTree = values.foldLeft(Dummy: IntTree) { case (tree, elem) => tree.insert(elem) }

  def unapplySeq(tree: IntTree): Option[Seq[Int]] = ???
}

object slide6_unapply_seq extends App {
  val tree: IntTree = IntTree(2, 4, 3, 1)

  tree match {
    case IntTree(1, 2, 3, elem) => println(s"last element is $elem")
  }

  tree match {
    case IntTree(1, 2, elems @ _*) => println(s"last elements are $elems")
  }
}
