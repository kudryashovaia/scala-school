package ru.tinkoff.fintech.seminars.seminar3

object unapply_case_class extends App {
  // Pattern-matching case-классов, tuple и т.п. использует метод unapply.
  // В кейс классах этот метод генерируется.

  // Класс Human в pattern-matching ведет себя так, как если бы он был case-классом
  // case class Human(age: Int, name: String)
  class Human(val age: Int, val name: String) {
    override def toString: String = s"Human($age, $name)"
    // equals, hashCode, copy etc.
  }

  object Human {
    def apply(age: Int, name: String): Human = {
      println(s"apply($age, $name)")
      new Human(age, name)
    }

    // На вход метод apply принимает "целый" объект, в этом случае case-класс
    // Возвращается Option от Tuple, который содержит "разобранный" аргумент,
    // в этом случае Tuple2 с типами параметров case-класса
    def unapply(arg: Human): Option[(Int, String)] = {
      println(s"unapply($arg)")
      Some((arg.age, arg.name))
    }
  }

  val human            = Human(23, "Alexander")
  val Human(age, name) = human
  println("sep")

  val maybeHuman: Option[Human] = Some(human)
  maybeHuman match {
    case Some(Human(age, name)) => println(s"Found $name who is $age years old")
    case None                   => println("Found no one")
  }
}

object custom_unapply extends App {
  // Метод unapply может быть определен в любом объекте. Объекты с методом unapply называются extractor objects.
  sealed trait SemiAxis extends Any

  class PositiveInteger private (val value: Int) extends AnyVal with SemiAxis
  class NegativeInteger private (val value: Int) extends AnyVal with SemiAxis

  object PositiveInteger {
    def unapply(value: Int): Option[Int] = if (value > 0) Some(value) else None
  }

  object NegativeInteger {
    def unapply(value: Int): Option[Int] = if (value < 0) Some(value) else None
  }

  object SemiAxis {
    // Напишите unapply здесь
  }

  def plot(int: Int): Unit =
    int match {
      case PositiveInteger(positive) => println(s"~$positive~~~>");
      case NegativeInteger(negative) => println(s"<~~~$negative~")
      case 0                         => println("~~0~~")
    }

  plot(100)
  plot(-12)
  plot(0)

  // Реализуйте SemiAxis.unapply так, чтобы код ниже работал

//  def divide(a: Int, b: Int): Either[String, Int] =
//    b match {
//      case SemiAxis(notNull) => Right(a / notNull)
//      case _ => Left("Dividing by null!")
//    }
//  divide(4, 2)
//  divide(4, 0)
}
