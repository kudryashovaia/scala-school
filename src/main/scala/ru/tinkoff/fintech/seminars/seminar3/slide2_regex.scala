package ru.tinkoff.fintech.seminars.seminar3

import ru.tinkoff.fintech.seminars.seminar3.slide2_regex.Baggage.{Amount, Weight, WeightUnit}
import cats.syntax.option._

object slide2_regex extends App {

  case class Baggage(count: Amount, weight: Weight)

  object Baggage {
    case class Weight(amount: Int, unit: WeightUnit)

    sealed trait Amount

    object Amount {
      case object Undefined        extends Amount
      case class Value(value: Int) extends Amount
    }

    sealed trait WeightUnit
    object WeightUnit {
      case object kg      extends WeightUnit
      case object unknown extends WeightUnit
    }
  }

  object regex {
    val limitedWeight         = "(\\d+)kg".r
    val limitedCount          = "(\\d+)bags".r
    val limitedCountAndWeight = "(\\d+)bags(\\d+)kg".r
  }

  def parseBaggage(baggage: String): Option[Baggage] =
    baggage match {
      case regex.limitedWeight(weight) => Baggage(Amount.Undefined, Weight(weight.toInt, WeightUnit.kg)).some
      case regex.limitedCount(count)   => Baggage(Amount.Value(count.toInt), Weight(1, WeightUnit.unknown)).some
      case regex.limitedCountAndWeight(count, weight) =>
        Baggage(Amount.Value(count.toInt), Weight(weight.toInt, WeightUnit.kg)).some
      case _ => None
    }

  println(parseBaggage("5bags10kg"))
  println(parseBaggage("5bags"))
  println(parseBaggage("50kg"))
  println(parseBaggage(""))
}
