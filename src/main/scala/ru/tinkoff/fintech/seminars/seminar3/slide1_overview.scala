package ru.tinkoff.fintech.seminars.seminar3


object slide1_overview extends App {

  val num: Int = 42

  // value matching
  num match {
    case 42 => println("num is 42")
    case _ => println("num is not 42")
  }

  // matching with guards
  num match {
    case x if x % 2 == 0 => println("num is even")
    case _ => println("num is odd")
  }
}


object slide1_any extends App {

  // matching by type
  def anyMatch(value: Any) = value match {
    case s: String => println(s"here is a string '$s'")
    case i: Int => println(s"here is an int '$i'")
    case _ => println("unknown type")
  }

  anyMatch(1)
  anyMatch("foo")
  anyMatch(3.14)
}


object slide1_product extends App {
  case class Student(name: String, age: Int, avgScore: Double)
  val alex = Student("Alex", 23, 7.0)

  alex match {
    case Student(nickname, age, _) => println(s"My name is $nickname and I am $age years old!")
  }


  val Student(name, _, avgScore) = alex


  val pair: (Int, String) = (42, "foo")

  val (int, string) = pair
}


object slide1_sealed extends App {

  sealed trait Mammal
  case object Cat extends Mammal
  case object Dolphin extends Mammal
  case class Human(age: Int) extends Mammal

  val mammal: Mammal = Human(67)

  def matchMammal(mammal: Mammal) = mammal match {
    case Cat => println("I am cat")
    case Dolphin => println("I am dolphin")
    case h: Human if h.age < 50 => println("I am young human")
    case h @ Human(age) if age > 50 => println(s"I am old human $h")
  }

  matchMammal(Human(67))

//   matchMammal(Human(50))

}

