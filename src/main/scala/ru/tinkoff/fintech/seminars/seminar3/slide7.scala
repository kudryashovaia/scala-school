package ru.tinkoff.fintech.seminars.seminar3

import cats.data.NonEmptyList
import cats.syntax.option._
import cats.syntax.either._

object Ops {

  implicit class NonEmptyListOps[A](val l: NonEmptyList[NonEmptyList[A]]) extends AnyVal {
    def transpose: NonEmptyList[NonEmptyList[A]] = {
      NonEmptyList.fromListUnsafe(l.map(_.toList).toList.transpose.map(NonEmptyList.fromListUnsafe))
    }
  }

}


object slide7 extends App {
  import Ops._

  println(NonEmptyList.of(NonEmptyList.of(1, 2, 3),
    NonEmptyList.of(1, 2, 3),
    NonEmptyList.of(1, 2, 3)).transpose)


  List(1, 2).some


  "foo".asLeft[Int]



  def makeList[A](values: A*): List[A] = values.toList

  makeList(1, 2, 3, 4)

  val l = List(1, 2, 3)

  makeList(l: _*)

}
