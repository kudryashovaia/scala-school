package ru.tinkoff.fintech.seminars.seminar3

object slide5_unapply_seq extends App {
  class NonEmptyList[A](val head: A, val tail: List[A])
  object NonEmptyList {
    def apply[A](head: A, rest: A*): NonEmptyList[A] = new NonEmptyList(head, rest.toList)
    def unapplySeq[A](list: NonEmptyList[A]): Option[Seq[A]] =
      Some(list.head :: list.tail)
  }

  def countArgs(nel: NonEmptyList[Int]): Unit =
    nel match {
      case NonEmptyList(a)       => println(s"one item: $a")
      case NonEmptyList(a, b)    => println(s"two items: $a, $b")
      case NonEmptyList(a, b, c) => println(s"three items: $a, $b, $c")
      case NonEmptyList()        => println(s"ooops")
      case _                     => println("too many items!")
    }

  countArgs(NonEmptyList(1))
  countArgs(NonEmptyList(1, 2))
  countArgs(NonEmptyList(1, 2, 3))
  countArgs(NonEmptyList(1, 2, 3, 4))
}
