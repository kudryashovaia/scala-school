package ru.tinkoff.fintech.homeworks.homework3

import org.scalatest.{FlatSpec, Matchers}

class task1_show_eq_test extends FlatSpec with Matchers {

  "Show instance" should "be visible everywhere" in {
    """
      | import cats.Show
      | implicitly[Show[ru.tinkoff.fintech.homeworks.homework3.task1_show_eq.Developer]]
      |""".stripMargin should compile
  }

  "Eq instance" should "be visible everywhere" in {
    """
      | import cats.Eq
      | implicitly[Eq[ru.tinkoff.fintech.homeworks.homework3.task1_show_eq.Developer]]
      |""".stripMargin should compile
  }
}
