package ru.tinkoff.fintech.homeworks.homework1

import org.scalatest._

class task2_map_test extends FlatSpec with Matchers {
  import task2_map.unionWith

  val map1 = Map("a" -> 1, "c" -> 3)
  val map2 = Map("b" -> 2, "c" -> 4)

  "unionWith" should "union with sum" in {
    unionWith(map1, map2)(_ + _) shouldBe Map("a" -> 1, "b" -> 2, "c" -> 7)
    unionWith(map2, map1)(_ + _) shouldBe Map("a" -> 1, "b" -> 2, "c" -> 7)
  }

  it should "union with product" in {
    unionWith(map1, map2)(_ * _) shouldBe Map("a" -> 1, "b" -> 2, "c" -> 12)
    unionWith(map2, map1)(_ * _) shouldBe Map("a" -> 1, "b" -> 2, "c" -> 12)
  }

  it should "union with discard right" in {
    unionWith(map1, map2)((x, y) => x) shouldBe Map("a" -> 1, "b" -> 2, "c" -> 3)
    unionWith(map2, map1)((x, y) => x) shouldBe Map("a" -> 1, "b" -> 2, "c" -> 4)
  }

  it should "union with discard left" in {
    unionWith(map1, map2)((x, y) => y) shouldBe Map("a" -> 1, "b" -> 2, "c" -> 4)
    unionWith(map2, map1)((x, y) => y) shouldBe Map("a" -> 1, "b" -> 2, "c" -> 3)
  }
}
