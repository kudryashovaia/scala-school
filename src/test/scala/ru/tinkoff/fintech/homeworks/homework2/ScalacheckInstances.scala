package ru.tinkoff.fintech.homeworks.homework2
import org.scalacheck.{Arbitrary, Cogen, Gen}
import ru.tinkoff.fintech.homeworks.homework2.Ternary.{Maybe, No, Yes}

object ScalacheckInstances {
  implicit val arbitraryThree: Arbitrary[Ternary] = Arbitrary(Gen.oneOf(Yes, No, Maybe))
  implicit val cogenThree: Cogen[Ternary] = Cogen {
    case Yes   => 1L
    case No    => 2L
    case Maybe => 3L
  }
}
