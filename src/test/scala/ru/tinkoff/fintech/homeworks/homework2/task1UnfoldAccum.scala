package ru.tinkoff.fintech.homeworks.homework2

import org.scalatest.prop.TableDrivenPropertyChecks
import org.scalatest.{Matchers, PropSpec}
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks
import cats.syntax.either._
import cats.syntax.option._

class task1UnfoldAccum extends PropSpec with TableDrivenPropertyChecks with Matchers {
  val unfolds = Table(
    ("start", "iter"),
    (0L, (x: Long) => Some((x.toInt, x + 1)).filter(_._2 < 100L)),
    (1000L, Function.const(None) _),
    (-100L, (x: Long) => Some((x.toInt * 1000, x + 1)).filter(_._2 > 0)),
    (-100L, (x: Long) => Some((x.toInt * 1000, x + 1)).filter(_._2 < 0)),
    (27L, (x: Long) => Some((x.toInt, if (x % 2 == 0) x / 2 else 3 * x + 1)).filter(_._2 > 1))
  )

  property("unfoldAccum should agree with unfold") {
    forAll(unfolds)(
      (start: Long, f: Long => Option[(Int, Long)]) => ListOps.unfoldAccum(start)(f)._1 should ===(List.unfold(start)(f))
    )
  }

  property("unfoldAccum should agree with last") {
    def last[A, S](s: S)(f: S => Option[(A, S)]): S = f(s) match {
      case None          => s
      case Some((_, s1)) => last(s1)(f)
    }

    forAll(unfolds)(
      (start: Long, f: Long => Option[(Int, Long)]) => ListOps.unfoldAccum(start)(f)._2 should ===(last(start)(f))
    )
  }
}

class task1FoldLeftEither extends PropSpec with ScalaCheckDrivenPropertyChecks with Matchers {
  property("foldLeftEither should accum lefts")(
    forAll(
      (list: List[Either[Int, Long]], l: Long, i: Int, f: (Long, Int) => Long, g: (Int, Long) => Int) =>
        ListOps.foldLeftEither(list)(l, i)(f)(g)._1 should ===(list.flatMap(_.left.toOption).foldLeft(l)(f))
    )
  )

  property("foldLeftEither should accum rights")(
    forAll(
      (list: List[Either[Int, Long]], l: Long, i: Int, f: (Long, Int) => Long, g: (Int, Long) => Int) =>
        ListOps.foldLeftEither(list)(l, i)(f)(g)._2 should ===(list.flatMap(_.toOption).foldLeft(i)(g))
    )
  )
}

class task1ListOpsHard extends PropSpec with ScalaCheckDrivenPropertyChecks with Matchers {
  import ru.tinkoff.fintech.homeworks.homework2.ListOpsHard.listTailRecM
  property("single equivalence") {
    forAll((a: Int, f: Int => Long) => listTailRecM(a)(a => List(Right(f(a)))) should ===(List(f(a))))
  }

  property("map equivalence") {
    forAll(
      (f: Int => Long, l: List[Int]) =>
        listTailRecM(none[Int]) {
          case None    => l.map(a => a.some.asLeft[Long])
          case Some(a) => List(f(a).asRight)
        } should ===(l.map(f))
    )
  }

  property("iterate equivalence") {
    forAll(
      (n: Short) =>
        listTailRecM(n)(i => List(if (i < 0) (i + 1).toShort.asLeft[Short]else i.asRight)) should ===(List(n.max(0)))
    )
  }
}
