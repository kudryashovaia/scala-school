package ru.tinkoff.fintech.homeworks.homework2
import org.scalatest.{Matchers, PropSpec}
import org.scalatest.prop.GeneratorDrivenPropertyChecks
import org.scalatestplus.scalacheck.ScalaCheckDrivenPropertyChecks

class task3_TrieVec extends PropSpec with ScalaCheckDrivenPropertyChecks with Matchers {

  override implicit val generatorDrivenConfig: PropertyCheckConfiguration = PropertyCheckConfiguration(
    minSuccessful = 50
  )

  property("constructed TrieVec is equivalent to List") {
    forAll((xs: List[Int]) => TrieVec(xs: _*).toList should ===(xs))
  }

  property("forall implemented") {
    forAll((xs: List[Int], p: Int => Boolean) => TrieVec(xs: _*).forall(p) should ===(xs.forall(p)))
  }

  property("exists implemented") {
    forAll((xs: List[Int], p: Int => Boolean) => TrieVec(xs: _*).exists(p) should ===(xs.exists(p)))
  }

  property("collectFirst implemented") {
    forAll(
      (xs: List[Int], p: PartialFunction[Int, Long]) => TrieVec(xs: _*).collectFirst(p) should ===(xs.collectFirst(p))
    )
  }

  property("take implemented") {
    forAll((xs: List[Int], n: Int) => TrieVec(xs: _*).take(n).toList should ===(xs.take(n)))
  }

  property("drop implemented") {
    forAll((xs: List[Int], n: Int) => TrieVec(xs: _*).drop(n).toList should ===(xs.drop(n)))
  }

  property("takeRight implemented") {
    forAll((xs: List[Int], n: Int) => TrieVec(xs: _*).takeRight(n).toList should ===(xs.takeRight(n)))
  }

  property("dropRight implemented") {
    forAll((xs: List[Int], n: Int) => TrieVec(xs: _*).dropRight(n).toList should ===(xs.dropRight(n)))
  }

  property("map implemented") {
    forAll((xs: List[Int], f: Int => Long) => TrieVec(xs: _*).map(f).toList should ===(xs.map(f)))
  }

  property("reversed implemented") {
    forAll((xs: List[Int]) => TrieVec(xs: _*).reverse should ===(xs.reverse))
  }

  property("tail implemented") {
    forAll((xs: List[Int], x: Int) => TrieVec((x +: xs): _*).tail.toList should ===(xs))
  }

  property("init implemented") {
    forAll((xs: List[Int], x: Int) => TrieVec((xs :+ x): _*).init.toList should ===(xs))
  }

}
