package ru.tinkoff.fintech
import ru.tinkoff.fintech.budget.categories.{CategoryData, CategoryId}
import ru.tinkoff.fintech.budget.entities.Entity
import ru.tinkoff.tschema.finagle.routing.ZioRouting
import zio.ZIO

package object budget {
  type Category = Entity[CategoryId, CategoryData]

  type Http[+A] = ZioRouting.ZIOHttp[ServerEnv, BudgetError, A]

  type Budget[+A] = ZIO[ServerEnv, BudgetError, A]
}
