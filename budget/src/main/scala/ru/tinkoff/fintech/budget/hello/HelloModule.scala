package ru.tinkoff.fintech.budget
package hello
import ru.tinkoff.fintech.budget.HttpModule
import ru.tinkoff.tschema.finagle.MkService
import ru.tinkoff.tschema.syntax
import ru.tinkoff.tschema.finagle.tethysInstances._
import ru.tinkoff.tschema.swagger.MkSwagger
import ru.tinkoff.tschema.syntax._
import ru.tinkoff.tschema.custom.syntax._
import zio.ZIO
import org.manatki.derevo.derive
import org.manatki.derevo.tethysInstances.{tethysReader, tethysWriter}
import org.manatki.derevo.tschemaInstances.swagger

object HelloModule extends HttpModule {
  @derive(tethysWriter, tethysReader, swagger)
  case class Hello(who: String, what: Option[String])

  def api = tagPrefix("hello") |> (
    (
      operation("world") |> get |> plain[String]
    ) ~ (
      operation("hello") |> queryParam[String]("who") |> get |> json[Hello]
    )
  )

  object handler {
    def world: Budget[String]     = ZIO.succeed("Hello lol!!!")
    def hello(who: String): Hello = Hello(who, Some("Hi!"))
  }

  val route = MkService[Http](api)(handler)
  def swag  = MkSwagger(api)
}
