package ru.tinkoff.fintech.budget

trait ZIOModule[X] {
  type Service[R]
  def > : Service[X]
}
