package ru.tinkoff.fintech.budget.entities

import java.time.{Instant, OffsetDateTime, ZonedDateTime}

import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger

@derive(encoder, decoder, swagger)
final case class Entity[Id, Data](
    id: Id,
    data: Data,
    created: ActionInfo,
    modified: Option[ActionInfo] = None,
)

@derive(encoder, decoder, swagger)
final case class ActionInfo(date: OffsetDateTime, user: Option[String])
