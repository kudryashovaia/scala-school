package ru.tinkoff.fintech.budget
import cats.instances.list._
import cats.syntax.foldable._
import cats.syntax.semigroupk._
import com.twitter.finagle.Http
import com.twitter.finagle.http.Response
import com.twitter.util.{Await, Duration}
import ru.tinkoff.fintech.budget.categories.CategoryModule
import ru.tinkoff.fintech.budget.hello.HelloModule
import ru.tinkoff.fintech.budget.swagger.Swagger
import ru.tinkoff.tschema.finagle.RunHttp
import ru.tinkoff.tschema.swagger.SwaggerBuilder
import zio.ZIO
import zio.console.{putStr, putStrLn}

object BudgetHttp {

  val port = 8888

  val host = "localhost"

  val modules: List[HttpModule] = List(HelloModule, CategoryModule)

  val service: Http[Response] = modules.foldMapK(_.route)

  val swaggerBuilder: SwaggerBuilder = modules.foldMap(_.swag)

}

object BudgetServer {
  import BudgetHttp._
  val server = for {
    srv  <- RunHttp.run[Budget](Swagger.route <+> service)
    list <- ZIO.effect(Http.serve(s"0.0.0.0:$port", srv))
    _    <- putStr(s"started at ${list.boundAddress} see  http://localhost:$port/swagger.php ")
    _    <- ZIO.effect(Await.ready(list, Duration.Top)).fork
    res  <- ZIO.never
  } yield res

  val runServer: ZIO[zio.ZEnv, Nothing, Unit] = for {
    env <- ServerEnv()
    _   <- server.provide(env).catchAll(error => putStrLn(error.toString))
  } yield ()

}
