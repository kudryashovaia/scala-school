package ru.tinkoff.fintech.budget.entities

import ru.tinkoff.fintech.budget.ZIOModule
import zio.URIO
import zio.clock._

trait Entities {
  val entities: Entities.Service[Any]
}

object Entities extends ZIOModule[Entities]{
  trait Service[R] {
    def makeEntity[Id, Data](id: Id, data: Data): URIO[R, Entity[Id, Data]]
  }

  object > extends Entities.Service[Entities] {
    def makeEntity[Id, Data](id: Id, data: Data): URIO[Entities, Entity[Id, Data]] =
      URIO.accessM(_.entities.makeEntity(id, data))
  }

  trait Live extends Service[Any] {
    val clock: Clock.Service[Any]

    def makeEntity[Id, Data](id: Id, data: Data): URIO[Any, Entity[Id, Data]] =
      clock.currentDateTime.map(dt => Entity(id, data, created = ActionInfo(dt, None)))
  }
}
