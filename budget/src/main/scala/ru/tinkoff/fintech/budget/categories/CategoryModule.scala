package ru.tinkoff.fintech.budget.categories

import com.twitter.finagle.http.Response
import ru.tinkoff.fintech.budget.{Budget, Http, HttpModule}
import ru.tinkoff.tschema.custom.JsonResult
import ru.tinkoff.tschema.custom.syntax._
import ru.tinkoff.tschema.finagle.MkService
import ru.tinkoff.tschema.swagger.{MkSwagger, SwaggerBuilder}
import ru.tinkoff.tschema.syntax._
import ru.tinkoff.tschema.typeDSL


object CategoryModule extends HttpModule {

  def api =
    tagPrefix("categories") |> (
      (key("getAll") |> get |> json[List[String]]) <>
        (key("addOne") |> post |> jsonBody[CategoryData]("data") |> json[CategoryId])
    )

  object handler {
    def getAll: Budget[List[String]] = Categories.>.getNames
    def addOne(data : CategoryData) : Budget[CategoryId] = Categories.>.putOne(data)
  }

  def route: Http[Response] =  MkService[Http](api)(handler)

  def swag: SwaggerBuilder = MkSwagger(api)
}
