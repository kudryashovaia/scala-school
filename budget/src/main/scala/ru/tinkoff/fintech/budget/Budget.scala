package ru.tinkoff.fintech.budget
import zio.console.putStrLn
import zio.{App, ZIO, console}

object Budget extends App {
  def run(args: List[String]): ZIO[zio.ZEnv, Nothing, Int] =
    BudgetServer.runServer as 1
}

class BudgetError(message: String) extends RuntimeException(message, null, false, false)
