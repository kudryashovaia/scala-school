package ru.tinkoff.fintech.budget
package categories
import java.util.UUID

import org.manatki.derevo.circeDerivation.{decoder, encoder}
import org.manatki.derevo.derive
import org.manatki.derevo.tschemaInstances.swagger
import ru.tinkoff.fintech.budget.entities.Entities
import zio.macros.annotation.accessible
import zio.{Ref, UIO, URIO, ZIO}

@derive(encoder, decoder, swagger)
final case class CategoryData(
    name: String,
    descripion: String
)

@derive(encoder, decoder, swagger)
final case class CategoryId(uuid: UUID)

@accessible(">")
trait Categories {
  val categories: Categories.Service[Any]
}

object Categories extends ZIOModule[Categories] {
  type State = Map[String, Category]

  val initialState: State = Map.empty

  trait Service[R] {
    def getNames: URIO[R, List[String]]
    def getOne(name: String): ZIO[R, NotFound, Category]
    def putOne(data: CategoryData): ZIO[R, AllreadyExists, CategoryId]
    def remove(name: String): ZIO[R, NotFound, Unit]
  }

  trait Live extends Service[Any] {

    val entities: Entities.Service[Any]
    val categories: Ref[State]

    def getNames: URIO[Any, List[String]] = categories.get.map(_.keys.toList)

    def getOne(name: String): ZIO[Any, NotFound, Category] =
      categories.get.map(_.get(name)).someOrFail(NotFound(name))

    def putOne(data: CategoryData): ZIO[Any, AllreadyExists, CategoryId] =
      for {
        uuid   <- UIO.effectTotal(UUID.randomUUID())
        id     = CategoryId(uuid)
        entity <- entities.makeEntity(id, data)
        _ <- categories
              .modify(m => if (m.contains(data.name)) (false, m) else (true, m + (data.name -> entity)))
              .reject { case false => AllreadyExists(data.name) }
      } yield id
    def remove(name: String): ZIO[Any, NotFound, Unit] =
      categories
        .modify(m => if (m.contains(name)) (true, m - name) else (false, m))
        .reject { case false => NotFound(name) }
        .unit
  }
}

sealed abstract class CategoryError(message: String) extends BudgetError(message)

final case class AllreadyExists(name: String) extends CategoryError(s"category with name $name already exists")
final case class NotFound(name: String)       extends CategoryError(s"category with name $name does not found")
