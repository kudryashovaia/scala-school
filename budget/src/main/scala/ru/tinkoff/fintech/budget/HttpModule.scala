package ru.tinkoff.fintech
package budget
import com.twitter.finagle.http.Response
import ru.tinkoff.tschema.swagger.SwaggerBuilder

trait HttpModule {
  def route: Http[Response]
  def swag: SwaggerBuilder
}
