package ru.tinkoff.fintech.budget
import cats.Monad
import ru.tinkoff.fintech.budget.categories.Categories
import ru.tinkoff.fintech.budget.categories.Categories.State
import ru.tinkoff.fintech.budget.entities.Entities
import zio.blocking.Blocking
import zio.clock.Clock
import zio.console.Console
import zio.{Ref, URIO, ZIO}

abstract class ServerEnv extends Clock with Entities with Categories with Blocking with Console

object ServerEnv {
  def apply(): URIO[zio.ZEnv, ServerEnv] =
    for {
      categoriesState <- Ref.make(Categories.initialState)
      context         <- ZIO.access[zio.ZEnv](x => x)
    } yield new ServerEnv {
      self =>

      val clock: Clock.Service[Any] = context.clock

      val console: Console.Service[Any] = context.console

      val blocking: Blocking.Service[Any] = context.blocking

      val entities: Entities.Service[Any] = new Entities.Live {
        val clock: Clock.Service[Any] = context.clock
      }

      val categories: Categories.Service[Any] = new Categories.Live {
        val categories: Ref[State]          = categoriesState
        val entities: Entities.Service[Any] = self.entities
      }
    }

  final implicit val exampleMonad: Monad[Budget] = zio.interop.catz.monadErrorInstance

  final implicit val httpMonad: Monad[Http] = zio.interop.catz.monadErrorInstance
}
